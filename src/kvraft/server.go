package raftkv

import (
	"encoding/gob"
	"labrpc"
	"log"
	"raft"
	"sync"
)

const Debug = 0

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

type Op struct {
	// Your definitions here.
	// Field names must start with capital letters,
	// otherwise RPC will break.
	Command string // "Get", "Put", or "Append"
	Key     string
	Value   string
	TransID int64
}

type RaftKV struct {
	mu      sync.Mutex
	me      int
	rf      *raft.Raft
	applyCh chan raft.ApplyMsg

	maxraftstate int // snapshot if log grows this big

	// Your definitions here.
	store    map[string]string
	index    int
	transLog map[int64]bool
	killed   bool
}

/* Helper functions */
func (kv *RaftKV) safeGetValue(key string) string {
	kv.mu.Lock()
	defer kv.mu.Unlock()
	return kv.store[key]
}

func (kv *RaftKV) safeReadLog(transID int64) bool {
	kv.mu.Lock()
	defer kv.mu.Unlock()
	return kv.transLog[transID]
}

func (kv *RaftKV) safeUpdate(key string, value string, transID int64) {
	kv.mu.Lock()
	if !kv.transLog[transID] {
		kv.store[key] = value
		kv.transLog[transID] = true
	}
	kv.mu.Unlock()
}

func (kv *RaftKV) tryToCommit(op Op) bool {
	index, _, isLeader := kv.rf.Start(op)
	if !isLeader {
		return false
	}

	// busy wait for command to be committed
	for !kv.killed && isLeader && kv.index < index {
		_, isLeader = kv.rf.GetState()
	}
	return isLeader
}

func (kv *RaftKV) Get(args *GetArgs, reply *GetReply) {
	// Your code here.
	op := Op{Command: "Get"}
	if kv.tryToCommit(op) {
		reply.WrongLeader = false
		reply.Value = kv.safeGetValue(args.Key)
	} else {
		reply.WrongLeader = true
	}
}

func (kv *RaftKV) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	// Your code here.
	op := Op{Command: args.Op, Key: args.Key, Value: args.Value, TransID: args.TransID}
	if kv.tryToCommit(op) {
		reply.WrongLeader = false
	} else {
		reply.WrongLeader = true
	}
}

//
// the tester calls Kill() when a RaftKV instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (kv *RaftKV) Kill() {
	kv.rf.Kill()
	// Your code here, if desired.
	kv.killed = true
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Raft to
// form the fault-tolerant key/value service.
// me is the index of the current server in servers[].
// the k/v server should store snapshots with persister.SaveSnapshot(),
// and Raft should save its state (including log) with persister.SaveRaftState().
// the k/v server should snapshot when Raft's saved state exceeds maxraftstate bytes,
// in order to allow Raft to garbage-collect its log. if maxraftstate is -1,
// you don't need to snapshot.
// StartKVServer() must return quickly, so it should start goroutines
// for any long-running work.
//
func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister, maxraftstate int) *RaftKV {
	// call gob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	gob.Register(Op{})

	kv := new(RaftKV)
	kv.me = me
	kv.maxraftstate = maxraftstate

	// Your initialization code here.
	kv.store = make(map[string]string)
	kv.index = 0
	kv.transLog = make(map[int64]bool)
	kv.killed = false

	kv.applyCh = make(chan raft.ApplyMsg)
	kv.rf = raft.Make(servers, me, persister, kv.applyCh)

	go func() {
		for !kv.killed {
			msg := <-kv.applyCh
			kv.index = msg.Index
			op, ok := msg.Command.(Op)
			if ok {
				switch op.Command {
				case "Get":
				case "Put":
					kv.safeUpdate(op.Key, op.Value, op.TransID)
				case "Append":
					kv.safeUpdate(op.Key, kv.safeGetValue(op.Key)+op.Value, op.TransID)
				}
			}
		}
	}()

	return kv
}
