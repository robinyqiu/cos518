package epaxosKV

import (
	"encoding/gob"
	"fmt"
	"labrpc"
	"raft"
	"sync"
)

type Op struct {
	ClientId int64
	SeqId    int64
	Op       string // "Get", "Append" or "Put"
	Key      string
	Value    string
}

type Request struct {
	responseCh chan interface{}
	op         Op
}

type ClientState struct {
	// The latest committed operation of the client. Used to guarantee
	// At-Most-Once semantics.
	latestCommitted int64

	// Used to track outstanding or previous operation
	latestSeen int64

	// Request type
	op string

	// Status flags
	// !submitted && !returned: latestSeen operation is not valid.
	// submitted && !returned: "Start" called, but response hasn't come back
	// submitted && returned: reponse is ready and cached.
	submitted bool
	returned  bool

	// Previous return
	retVal interface{}

	// People waiting for the response
	waitinglist []chan interface{}
}

type EpaxosKV struct {
	mu      sync.Mutex
	me      int
	ep      *Epaxos
	applyCh chan ApplyMsg

	// The state of each client is tracked
	clientStates map[int64]ClientState

	// the actual map
	kvmap map[string]string

	// request channel
	requestCh chan Request

	// Debug use
	debug int
}

// debug information
func (kv *EpaxosKV) debugPrint(s string, v ...interface{}) {
	if kv.debug > 0 {
		fmt.Printf("[Debug][%v] %v\n", kv.me, fmt.Sprintf(s, v...))
	}
}

func (kv *EpaxosKV) Get(args *GetArgs, reply *GetReply) {
	kv.debugPrint("[Get] request received: %v", args)
	op := Op{
		ClientId: args.ClientId,
		SeqId:    args.SeqId,
		Op:       "Get",
		Key:      args.Key,
	}
	responseCh := make(chan interface{})

	go func() {
		kv.requestCh <- Request{
			op:         op,
			responseCh: responseCh,
		}
	}()

	ret := <-responseCh
	*reply = ret.(GetReply)
	kv.debugPrint("[Get] request{%v} responded: %v", args, reply)
	return
}

func (kv *EpaxosKV) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	kv.debugPrint("[P/A] request received: %v", args)
	op := Op{
		ClientId: args.ClientId,
		SeqId:    args.SeqId,
		Op:       args.Op,
		Key:      args.Key,
		Value:    args.Value,
	}
	responseCh := make(chan interface{})

	go func() {
		kv.requestCh <- Request{
			op:         op,
			responseCh: responseCh,
		}
	}()

	ret := <-responseCh
	*reply = ret.(PutAppendReply)
	kv.debugPrint("[P/A] request{%v} responded: %v", args, reply)
	return
}

//
// the tester calls Kill() when a EpaxosKV instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (kv *EpaxosKV) Kill() {
	kv.debug = 0
}

func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister) *EpaxosKV {
	// call gob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	gob.Register(Op{})

	kv := new(EpaxosKV)
	kv.me = me

	kv.applyCh = make(chan ApplyMsg)
	kv.ep = Make(servers, me, persister, kv.applyCh)

	kv.clientStates = make(map[int64]ClientState)
	kv.kvmap = make(map[string]string)
	kv.requestCh = make(chan Request)
	kv.debug = globalDebugOn

	go kv.mainThread()

	return kv
}

func (kv *EpaxosKV) mainThread() {
	for {
		select {
		case req := <-kv.requestCh:
			if _, ok := kv.clientStates[req.op.ClientId]; !ok {
				kv.debugPrint("[Main] client state created for %v", req.op.ClientId)
				state := ClientState{
					latestCommitted: -1,
					latestSeen:      -1,
					submitted:       false,
					returned:        false,
				}
				kv.clientStates[req.op.ClientId] = state
			}
			state := kv.clientStates[req.op.ClientId]

			if req.op.SeqId < state.latestSeen {
				// A stale request.
				kv.debugPrint("[Main] stale op: %v", req.op)
				if req.op.Op == "Get" {
					req.responseCh <- GetReply{
						Err: ErrStaleRequest,
					}
				} else {
					req.responseCh <- PutAppendReply{
						Err: ErrStaleRequest,
					}
				}
			} else if req.op.SeqId == state.latestSeen {
				if !state.submitted {
					kv.debugPrint("[Main] duplicated unsubmitted op: %v", req.op)
					// Yet the request is not submitted.
					// Two possible reasons for this:
					//		I've never heard of this client before.
					//		Previously lost leadership after submitting.

					// Should be exactly the same request
					if req.op.Op != state.op {
						panic("[Fatal] Inconsistent record!")
					}

					// Submit a new request to raft
					kv.ep.Start(req.op, req.op.Key)
					state.submitted = true
					state.returned = false
					state.waitinglist = []chan interface{}{req.responseCh}
					kv.debugPrint("[Main] op re-submitted: %v", req.op)
					kv.clientStates[req.op.ClientId] = state
				} else {
					if state.returned {
						// Already returned, use the previous return value
						kv.debugPrint("[Main] duplicated returned op: %v", req.op)
						req.responseCh <- state.retVal
					} else {
						// Submitted but not returned, join waiting list
						kv.debugPrint("[Main] duplicated submitted op: %v", req.op)
						state.waitinglist = append(state.waitinglist, req.responseCh)
						kv.clientStates[req.op.ClientId] = state
					}
				}
			} else {
				// A new request.
				kv.debugPrint("[Main] new op: %v", req.op)

				// Notify everybody waiting
				if state.submitted && !state.returned {
					var reply interface{}
					if state.op == "Get" {
						reply = GetReply{
							Err: ErrStaleRequest,
						}
					} else {
						reply = PutAppendReply{
							Err: ErrStaleRequest,
						}
					}

					for _, responseCh := range state.waitinglist {
						responseCh <- reply
					}
				}

				// write-ahead
				state.latestSeen = req.op.SeqId
				state.op = req.op.Op
				state.submitted = false
				kv.clientStates[req.op.ClientId] = state

				// Submit a new request to raft
				kv.ep.Start(req.op, req.op.Key)
				state.submitted = true
				state.returned = false
				state.waitinglist = []chan interface{}{req.responseCh}
				kv.debugPrint("[Main] op submitted: %v", req.op)
				kv.clientStates[req.op.ClientId] = state
			}
		case msg := <-kv.applyCh:
			op := msg.Command.(Op)
			if _, ok := kv.clientStates[op.ClientId]; !ok {
				kv.debugPrint("[Main] client state created for %v", op.ClientId)
				state := ClientState{
					latestCommitted: -1,
					latestSeen:      -1,
					submitted:       false,
					returned:        false,
				}
				kv.clientStates[op.ClientId] = state
			}
			state := kv.clientStates[op.ClientId]

			// update kvmap
			if op.SeqId > state.latestCommitted {
				kv.debugPrint("[Main] new op committed: %v", op)
				// valid commitment
				switch op.Op {
				case "Get":
					if value, ok := kv.kvmap[op.Key]; ok {
						op.Value = value
					} else {
						op.Value = ""
					}
				case "Append":
					kv.kvmap[op.Key] += op.Value
				case "Put":
					kv.kvmap[op.Key] = op.Value
				default:
					panic("[Fatal] Unknown operation returned by raft.")
				}
			} else if op.SeqId == state.latestCommitted {
				kv.debugPrint("[Main] duplicated op committed: %v", op)
				if op.Op == "Get" {
					if value, ok := kv.kvmap[op.Key]; ok {
						op.Value = value
					} else {
						op.Value = ""
					}
				}
			} else {
				panic("[Fatal] Stale return from Raft!")
			}

			// update client status
			state.latestCommitted = op.SeqId

			// return to the waiting threads
			if state.submitted && !state.returned && state.latestCommitted >= state.latestSeen {
				var reply interface{}
				if state.latestCommitted == state.latestSeen {
					// this is what they're waiting for
					if op.Op == "Get" {
						reply = GetReply{
							Err:   OK,
							Value: op.Value,
						}
					} else {
						reply = PutAppendReply{
							Err: OK,
						}
					}
				} else if state.latestCommitted > state.latestSeen {
					// they're waiting for a stale request
					if state.op == "Get" {
						reply = GetReply{
							Err: ErrStaleRequest,
						}
					} else {
						reply = PutAppendReply{
							Err: ErrStaleRequest,
						}
					}

				}

				// tell them!
				for _, responseCh := range state.waitinglist {
					responseCh <- reply
				}

				// now clear the waitinglist and set the state
				state.returned = true
				state.retVal = reply
			}

			kv.clientStates[op.ClientId] = state
		}
	}
}
