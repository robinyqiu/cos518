package epaxosKV

const (
	OK              = "OK"
	ErrNoKey        = "ErrNoKey"
	ErrStaleRequest = "ErrStaleRequest"
	ErrRPCFailure   = "ErrRPCFailure"
)

type Err string

// Put or Append
type PutAppendArgs struct {
	ClientId int64
	SeqId    int64
	Key      string
	Value    string
	Op       string // "Put" or "Append"
}

type PutAppendReply struct {
	Err Err
}

type GetArgs struct {
	ClientId int64
	SeqId    int64
	Key      string
}

type GetReply struct {
	Err   Err
	Value string
}
