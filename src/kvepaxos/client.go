package epaxosKV

import "labrpc"
import "crypto/rand"
import "math/big"
import "time"

const (
	requestTimeout = 300 * time.Millisecond
)

type Clerk struct {
	servers  []*labrpc.ClientEnd
	clientId int64
	seqId    int64
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers

	ck.clientId = nrand()
	ck.seqId = 0
	return ck
}

//
// fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("RaftKV.Get", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) Get(key string) string {
	r := int(nrand())
	if r < 0 {
		r = -r
	}
	server := r % len(ck.servers)

	args := GetArgs{
		ClientId: ck.clientId,
		SeqId:    ck.seqId,
		Key:      key,
	}
	ck.seqId += 1

	for {
		replyCh := make(chan GetReply)
		go func(server int, replyCh chan GetReply) {
			reply := GetReply{}
			if ok := ck.servers[server].Call("EpaxosKV.Get", &args, &reply); ok {
				replyCh <- reply
			} else {
				replyCh <- GetReply{
					Err: ErrRPCFailure,
				}
			}
		}(server, replyCh)

		select {
		case reply := <-replyCh:
			if reply.Err == OK {
				return reply.Value
			}
			// else unlimited retries
		case <-time.After(requestTimeout):
			// unlimited retries
		}
	}
}

//
// shared by Put and Append.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("RaftKV.PutAppend", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) PutAppend(key string, value string, op string) {
	r := int(nrand())
	if r < 0 {
		r = -r
	}
	server := r % len(ck.servers)

	args := PutAppendArgs{
		ClientId: ck.clientId,
		SeqId:    ck.seqId,
		Op:       op,
		Key:      key,
		Value:    value,
	}
	ck.seqId += 1

	for {
		replyCh := make(chan PutAppendReply)

		go func(server int, replyCh chan PutAppendReply) {
			reply := PutAppendReply{}
			if ok := ck.servers[server].Call("EpaxosKV.PutAppend", &args, &reply); ok {
				replyCh <- reply
			} else {
				replyCh <- PutAppendReply{
					Err: ErrRPCFailure,
				}
			}
		}(server, replyCh)

		select {
		case reply := <-replyCh:
			if reply.Err == OK {
				return
			}
			// else unlimited retries
		case <-time.After(requestTimeout):
			// unlimited retries
		}
	}
}

func (ck *Clerk) Put(key string, value string) {
	ck.PutAppend(key, value, "Put")
}
func (ck *Clerk) Append(key string, value string) {
	ck.PutAppend(key, value, "Append")
}
