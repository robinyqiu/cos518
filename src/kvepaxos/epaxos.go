package epaxosKV

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"labrpc"
	"raft"
	"sync"
)

const (
	globalDebugOn = 0
)

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Type definition //////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// InstanceState
type InstanceState int

const (
	PreAccepted = iota
	Accepted
	Committed
	Executed
)

// Ballot
type BallotId struct {
	ServerId int
	Seq      int
}

func (b *BallotId) gt(c BallotId) bool {
	if b.Seq > c.Seq {
		return true
	} else if b.Seq == c.Seq && b.ServerId != c.ServerId {
		return true
	} else {
		return false
	}
}

// InstanceId
type InstanceId struct {
	ServerId   int
	InstanceId int
}

// Dependence List
type DepList []InstanceId

func union(a DepList, b DepList) (c DepList) {
	m := map[InstanceId]bool{}
	for _, i := range a {
		m[i] = true
	}
	for _, i := range b {
		m[i] = true
	}
	c = make(DepList, len(m))
	idx := 0
	for i := range m {
		c[idx] = i
		idx++
	}
	return
}

// Command
type Command struct {
	Cmd  interface{}
	Seq  int
	Deps DepList
}

func (c *Command) equal(b Command) bool {
	return c.Seq == b.Seq && c.Cmd == b.Cmd &&
		len(union(c.Deps, b.Deps)) == len(b.Deps)
}

// Instance
type Instance struct {
	Valid  bool
	State  InstanceState
	Ballot BallotId
	Id     InstanceId
	Key    string
	Cmd    Command
}

// ApplyMsg
type ApplyMsg struct {
	Id      InstanceId
	Command interface{}
}

// Epaxos
type Epaxos struct {
	mu        sync.Mutex
	peers     []*labrpc.ClientEnd
	persister *raft.Persister
	me        int
	applyCh   chan ApplyMsg

	cmds              [][]Instance
	interf            map[string]DepList
	latestAccepted    map[string]int
	latestPreaccepted map[string]int

	execWaitlist  map[InstanceId][]chan bool
	execQueue     []ApplyMsg
	execTokenChan chan bool

	debug int
}

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Epaxos Interfaces /////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// make Epaxos
func Make(peers []*labrpc.ClientEnd, me int, persister *raft.Persister, applyCh chan ApplyMsg) (ep *Epaxos) {
	gob.Register(Instance{})

	ep = &Epaxos{}
	ep.peers = peers
	ep.persister = persister
	ep.me = me
	ep.applyCh = applyCh
	ep.cmds = make([][]Instance, len(peers))
	for i := range ep.cmds {
		ep.cmds[i] = []Instance{}
	}
	ep.interf = make(map[string]DepList)
	ep.latestAccepted = map[string]int{}
	ep.latestPreaccepted = map[string]int{}
	ep.execWaitlist = make(map[InstanceId][]chan bool)
	ep.execQueue = []ApplyMsg{}
	ep.execTokenChan = make(chan bool)
	ep.debug = globalDebugOn

	ep.readPersist(persister.ReadRaftState())

	go ep.applyThread()

	return
}

func (ep *Epaxos) Start(cmd interface{}, key string) (id InstanceId) {
	ep.mu.Lock()
	defer ep.mu.Unlock()

	id = InstanceId{ep.me, len(ep.cmds[ep.me])}
	command := Command{cmd, 1, DepList{}}
	ep.updateAttributes(&command, key)

	inst := Instance{
		Valid:  true,
		State:  PreAccepted,
		Ballot: BallotId{ep.me, 0},
		Id:     id,
		Key:    key,
		Cmd:    command,
	}

	ep.insertInstance(inst)

	go ep.sendPreAcceptToAll(inst, false)
	return
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Epaxos Helpers //////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// debug print
func (ep *Epaxos) dprint(f string, v ...interface{}) {
	if ep.debug > 0 {
		fmt.Printf("[Debug][%v] %v\n", ep.me, fmt.Sprintf(f, v...))
	}
}

// persist & readPersist
func (ep *Epaxos) persist() {
	w := new(bytes.Buffer)
	e := gob.NewEncoder(w)
	err := e.Encode(ep.cmds)
	if err == nil {
		err = e.Encode(ep.interf)
	}
	if err == nil {
		err = e.Encode(ep.latestAccepted)
	}
	if err == nil {
		err = e.Encode(ep.latestPreaccepted)
	}
	if err != nil {
		panic(fmt.Sprintf("[Error][%v] Persistent storage failed.", ep.me))
	} else {
		ep.persister.SaveRaftState(w.Bytes())
		ep.dprint("Persister storage success.")
	}
}

func (ep *Epaxos) readPersist(data []byte) {
	r := bytes.NewBuffer(data)
	d := gob.NewDecoder(r)
	d.Decode(&ep.cmds)
	d.Decode(&ep.interf)
	d.Decode(&ep.latestAccepted)
	d.Decode(&ep.latestPreaccepted)
	ep.dprint("Persister read success.")
}

// update attributes of a command
func (ep *Epaxos) updateAttributes(command *Command, key string) {
	if v, ok := ep.interf[key]; ok {
		command.Deps = union(command.Deps, v)
		command.Seq = ep.latestPreaccepted[key] + 1
	}
}

// insert a new instance, and update interf/latestAccpted/latestPreaccepted
func (ep *Epaxos) insertInstance(inst Instance) {
	// insert first.
	id := inst.Id
	length := len(ep.cmds[id.ServerId])
	if length > id.InstanceId {
		ep.cmds[id.ServerId][id.InstanceId] = inst
	} else {
		appendings := make([]Instance, id.InstanceId-length)
		for i := range appendings {
			appendings[i].Id = InstanceId{ep.me, length + i}
		}
		appendings = append(appendings, inst)
		ep.cmds[id.ServerId] = append(ep.cmds[id.ServerId], appendings...)
	}

	// update records
	if inst.State > PreAccepted {
		if arr, ok := ep.interf[inst.Key]; ok {
			if inst.Cmd.Seq > ep.latestAccepted[inst.Key] {
				ep.latestAccepted[inst.Key] = inst.Cmd.Seq
				if inst.Cmd.Seq > ep.latestPreaccepted[inst.Key] {
					ep.latestPreaccepted[inst.Key] = inst.Cmd.Seq
				}
				newInterf := DepList{id}
				for _, iid := range arr {
					v, _ := ep.getInstance(iid)
					if v.Cmd.Seq > inst.Cmd.Seq {
						newInterf = append(newInterf, iid)
					}
				}
				ep.interf[inst.Key] = newInterf
			}
		} else {
			ep.interf[inst.Key] = DepList{}
			ep.latestAccepted[inst.Key] = inst.Cmd.Seq
			ep.latestPreaccepted[inst.Key] = inst.Cmd.Seq
		}
	} else {
		if arr, ok := ep.interf[inst.Key]; ok {
			ep.interf[inst.Key] = union(arr, DepList{id})
			if inst.Cmd.Seq > ep.latestPreaccepted[inst.Key] {
				ep.latestPreaccepted[inst.Key] = inst.Cmd.Seq
			}
		} else {
			ep.interf[inst.Key] = DepList{id}
			ep.latestAccepted[inst.Key] = 0
			ep.latestPreaccepted[inst.Key] = inst.Cmd.Seq
		}
	}

	ep.persist()
}

// get an instance
func (ep *Epaxos) getInstance(id InstanceId) (inst Instance, ok bool) {
	if len(ep.cmds[id.ServerId]) <= id.InstanceId {
		inst = Instance{}
		ok = false
	} else {
		inst = ep.cmds[id.ServerId][id.InstanceId]
		ok = inst.Valid
	}
	return
}

// RPC sender
type ProtocolReply struct {
	Server int
	Ok     bool
	Inst   Instance
}

func (ep *Epaxos) sendRPC(server int, rpc string, inst Instance, replyCh chan ProtocolReply) {
	reply := Instance{}
	ok := ep.peers[server].Call(rpc, inst, &reply)
	replyCh <- ProtocolReply{server, ok, reply}
}

// Register waitlist for a command to execute
func (ep *Epaxos) registerWaitlist(id InstanceId, q chan bool) {
	if qs, ok := ep.execWaitlist[id]; ok {
		ep.execWaitlist[id] = append(qs, q)
	} else {
		ep.execWaitlist[id] = []chan bool{q}
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Epaxos Phases ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Phase 1. Pre-Accept
func (ep *Epaxos) sendPreAcceptToAll(inst Instance, avoidFastPath bool) {
	// Go fast quorum first
	all := len(ep.peers)
	slow := all/2 + 1
	fast := all - 1
	replyCh := make(chan ProtocolReply, all) // there might be messages abandoned

	ep.dprint("[PreAccept] Request: {%v}.", inst)

	for i := range ep.peers {
		if i != ep.me {
			go ep.sendRPC(i, "Epaxos.PreAccept", inst, replyCh)
		}
	}

	same := !avoidFastPath
	cmd := inst.Cmd
	lostRPC := 0
	for count := 1; (same && count < fast) || (!same && count < slow); {
		bundle := <-replyCh
		reply := bundle.Inst
		if bundle.Ok {
			count++
			if reply.Ballot.gt(inst.Ballot) {
				// This instance is owned by others, which is weird, but we just
				// abandon here.
				ep.dprint("Pre-accept proposal{%v} replied with overwritten{%v}.", inst, reply)
				return
			} else if reply.State != PreAccepted {
				// This is instance is already accepted or committed.
				ep.dprint("Pre-accept proposal{%v} replied without pre-accept{%v}.", inst, reply)
				return
			}
			if same && !reply.Cmd.equal(inst.Cmd) {
				same = false
			}
			if !same {
				cmd.Deps = union(cmd.Deps, reply.Cmd.Deps)
				if reply.Cmd.Seq > cmd.Seq {
					cmd.Seq = reply.Cmd.Seq
				}
			}
		} else {
			if lostRPC < all-fast {
				lostRPC++
			} else {
				same = false
			}
			go ep.sendRPC(bundle.Server, "Epaxos.PreAccept", inst, replyCh)
		}
	}

	ep.mu.Lock()
	defer ep.mu.Unlock()

	// we need to check if I myself promised others first.
	inst.Cmd = cmd
	if oldInst, ok := ep.getInstance(inst.Id); ok {
		if oldInst.Ballot.gt(inst.Ballot) {
			ep.dprint("Pre-accept proposal{%v} outdated by {%v}.", inst, oldInst)
			return
		} else {
			if same {
				// pre-accept successfully
				ep.dprint("[PreAccept] Fast path: {%v}.", inst)
				inst.State = Committed
				ep.insertInstance(inst)
				go ep.sendCommitToAll(inst)
				go ep.preExecute(inst.Id)
			} else {
				// we need to go through accept
				ep.dprint("[PreAccept] Slow path: {%v}.", inst)
				inst.State = Accepted
				ep.insertInstance(inst)
				go ep.sendAcceptToAll(inst)
			}
		}
	} else {
		panic("Pre-accept proposal disappeared!")
	}
}

func (ep *Epaxos) PreAccept(arg Instance, reply *Instance) {
	ep.mu.Lock()
	defer ep.mu.Unlock()

	// Step 1. Forward reply if promised to others
	if inst, ok := ep.getInstance(arg.Id); ok {
		*reply = inst
		if reply.State > PreAccepted || reply.Ballot.gt(arg.Ballot) {
			// I've given someone else a promise.
			ep.dprint("[PreAccept Reply] Arg: {%v}, Ret: {%v}.", arg, *reply)
			return
		}
	}

	// Step 2. Update deps
	*reply = arg
	ep.updateAttributes(&reply.Cmd, reply.Key)

	// Step 3. Store this in cmds log
	ep.insertInstance(*reply)
	ep.dprint("[PreAccept Reply] Arg: {%v}, Ret: {%v}.", arg, *reply)
	return
}

// Phase 2. Accept
func (ep *Epaxos) sendAcceptToAll(inst Instance) {
	all := len(ep.peers)
	slow := all/2 + 1
	replyCh := make(chan ProtocolReply, all)

	for i := range ep.peers {
		if i != ep.me {
			go ep.sendRPC(i, "Epaxos.Accept", inst, replyCh)
		}
	}

	for count := 1; count < slow; {
		bundle := <-replyCh
		reply := bundle.Inst
		if bundle.Ok {
			count++
			if reply.Ballot.gt(inst.Ballot) {
				ep.dprint("Accept proposal{%v} replied with overwritten{%v}.", inst, reply)
				return
			} else if reply.State != Accepted {
				ep.dprint("Accept proposal{%v} replied without accept{%v}.", inst, reply)
				return
			}

			if !reply.Cmd.equal(inst.Cmd) {
				panic("Accept returns with unequal instance!")
			}
		} else {
			go ep.sendRPC(bundle.Server, "Epaxos.Accept", inst, replyCh)
		}
	}

	ep.mu.Lock()
	defer ep.mu.Unlock()

	// we need to check if I myself promised others first.
	if oldInst, ok := ep.getInstance(inst.Id); ok {
		if oldInst.Ballot.gt(inst.Ballot) {
			ep.dprint("Accept proposal{%v} outdated by {%v}.", inst, oldInst)
			return
		} else {
			inst.State = Committed
			ep.insertInstance(inst)
			go ep.sendCommitToAll(inst)
			go ep.preExecute(inst.Id)
		}
	} else {
		panic("Accept proposal disappeared!")
	}
}

func (ep *Epaxos) Accept(arg Instance, reply *Instance) {
	ep.mu.Lock()
	defer ep.mu.Unlock()

	// Step 1. Forward reply if promised to others
	if inst, ok := ep.getInstance(arg.Id); ok {
		*reply = inst
		if reply.State > Accepted || reply.Ballot.gt(arg.Ballot) {
			// I've given someone else a promise.
			return
		} else if arg.Cmd.Seq <= ep.latestAccepted[arg.Key] {
			reply.State = PreAccepted
		}
	} else if v, ok := ep.latestAccepted[arg.Key]; ok {
		if arg.Cmd.Seq <= v {
			*reply = arg
			reply.State = PreAccepted
			return
		}
	}

	// Step 2. Store this in cmds log
	*reply = arg
	ep.insertInstance(*reply)
	return
}

// Phase 3. Commit
func (ep *Epaxos) sendCommitToAll(inst Instance) {
	replyCh := make(chan ProtocolReply, len(ep.peers))
	ep.dprint("[Commit] Request: {%v}.", inst)

	// notify everybody just once.
	for i := range ep.peers {
		if i != ep.me {
			go ep.sendRPC(i, "Epaxos.Commit", inst, replyCh)
		}
	}
}

func (ep *Epaxos) Commit(arg Instance, reply *Instance) {
	ep.mu.Lock()
	defer ep.mu.Unlock()

	// Step 1. Forward arg to reply
	if inst, ok := ep.getInstance(arg.Id); ok {
		*reply = inst
		if inst.State == Executed || inst.State == Committed {
			if !inst.Cmd.equal(arg.Cmd) {
				panic("Commit proposal doesn't match already commited/executed instance!")
			}
			ep.dprint("[Commit] Arg: {%v}, Ret: {%v}.", arg, *reply)
			return
		}
	}

	// Step 2. Store this in cmds log
	*reply = arg
	ep.insertInstance(*reply)
	ep.dprint("[Commit] Arg: {%v}, Ret: {%v}.", arg, *reply)
	return
}

// Phase 4. Explicit Prepare
func (ep *Epaxos) sendPrepareToAll(inst Instance) {
	all := len(ep.peers)
	slow := all/2 + 1
	replyCh := make(chan ProtocolReply, all)

	// only the ballot and id are useful here.
	ballot := BallotId{
		ServerId: ep.me,
		Seq:      inst.Ballot.Seq + 1,
	}
	reqInst := Instance{
		Valid:  true,
		Ballot: ballot,
		Id:     inst.Id,
		State:  PreAccepted,
	}

	ep.Prepare(reqInst, &inst)

	for i := range ep.peers {
		if i != ep.me {
			go ep.sendRPC(i, "Epaxos.Prepare", reqInst, replyCh)
		}
	}

	collection := []Instance{}
	if inst.Valid {
		collection = []Instance{inst}
	}
	initiatorReplied := false
	for count := 1; count < slow; {
		bundle := <-replyCh
		reply := bundle.Inst
		if bundle.Ok {
			count++
			if reply.Valid {
				if reply.Ballot.gt(ballot) {
					ep.dprint("Prepare proposal{%v} replied with overwritten{%v}.", reqInst, reply)
					return
				}
				collection = append(collection, reply)
			}
			if bundle.Server == reqInst.Id.ServerId {
				initiatorReplied = true
			}
		} else {
			go ep.sendRPC(bundle.Server, "Epaxos.Prepare", reqInst, replyCh)
		}
	}

	high := BallotId{
		ServerId: 0,
		Seq:      -1,
	}
	consensus := 0
	for _, item := range collection {
		if item.State >= Committed {
			// committed or executed!
			reqInst = item
			reqInst.Ballot = BallotId{
				ServerId: ep.me,
				Seq:      item.Ballot.Seq + 1,
			}
			reqInst.State = Committed
			break
		} else if item.State == Accepted {
			if consensus != -1 || item.Ballot.gt(high) {
				high = BallotId{
					ServerId: ep.me,
					Seq:      item.Ballot.Seq + 1,
				}
				reqInst = item
				reqInst.Ballot = high
				consensus = -1
			}
		} else if item.State == PreAccepted {
			if consensus == -1 {
				continue
			} else if consensus >= 0 {
				if item.Ballot.ServerId == reqInst.Id.ServerId && item.Ballot.Seq == 0 {
					if consensus == 0 {
						reqInst = item
						reqInst.Ballot = BallotId{
							ServerId: ep.me,
							Seq:      item.Ballot.Seq + 1,
						}
						consensus++
					} else {
						if !item.Cmd.equal(reqInst.Cmd) {
							consensus = -2
						} else {
							consensus++
						}
					}
				} else {
					consensus = -2
				}
			}
			if consensus == -2 && item.Ballot.gt(high) {
				high = BallotId{
					ServerId: ep.me,
					Seq:      item.Ballot.Seq + 1,
				}
				reqInst = item
				reqInst.Ballot = high
			}
		}
	}
	if initiatorReplied && consensus > 0 {
		consensus--
	}

	ep.mu.Lock()
	defer ep.mu.Unlock()

	// we need to check if I myself changed
	if oldInst, ok := ep.getInstance(reqInst.Id); ok {
		if oldInst.Ballot.gt(inst.Ballot) {
			ep.dprint("Prepare proposal{%v} outdated by {%v}.", inst, oldInst)
			return
		}
	}

	// choose next step according to the result
	ep.insertInstance(reqInst)
	if reqInst.State == Committed {
		go ep.sendCommitToAll(reqInst)
		go ep.preExecute(reqInst.Id)
	} else if reqInst.State == Accepted {
		go ep.sendAcceptToAll(reqInst)
	} else if reqInst.State == PreAccepted {
		if consensus >= all/2 {
			go ep.sendAcceptToAll(reqInst)
		} else {
			go ep.sendPreAcceptToAll(reqInst, true)
		}
	}
}

func (ep *Epaxos) Prepare(arg Instance, reply *Instance) {
	ep.mu.Lock()
	defer ep.mu.Unlock()

	if inst, ok := ep.getInstance(arg.Id); ok {
		*reply = inst
	} else {
		*reply = Instance{}
	}
	return
}

// Phase 5. Execute
func (ep *Epaxos) preExecute(id InstanceId) {
	ep.mu.Lock()
	defer ep.mu.Unlock()

	if inst, ok := ep.getInstance(id); ok && inst.State >= Committed {
		if inst.State == Executed {
			ep.dprint("[PreExecute] Already executed {%v}.", inst)
			return
		}

		ep.dprint("[PreExecute] {%v}.", inst)
		waitlist := []chan bool{}
		cmd := inst.Cmd
		for _, depid := range cmd.Deps {
			if dep, ok := ep.getInstance(depid); ok {
				if dep.Cmd.Seq > inst.Cmd.Seq {
					continue
				} else if dep.State == Executed {
					continue
				} else if dep.State == Committed {
					q := make(chan bool)
					ep.registerWaitlist(depid, q)
					waitlist = append(waitlist, q)
					go ep.preExecute(depid)
				} else {
					q := make(chan bool)
					ep.registerWaitlist(depid, q)
					waitlist = append(waitlist, q)
					ep.force(depid)
				}
			} else {
				q := make(chan bool)
				ep.registerWaitlist(depid, q)
				waitlist = append(waitlist, q)
				ep.force(depid)
			}
		}
		go ep.execute(id, waitlist)
	} else {
		panic("Propose to execute a non-existing, not committed, or executed instance!")
	}
}

func (ep *Epaxos) force(id InstanceId) {
	if inst, ok := ep.getInstance(id); ok {
		go ep.sendPrepareToAll(inst)
	} else {
		inst = Instance{
			Id: id,
		}
		go ep.sendPrepareToAll(inst)
	}
}

func (ep *Epaxos) execute(id InstanceId, waitlist []chan bool) {
	for _, q := range waitlist {
		<-q
	}

	ep.mu.Lock()
	defer ep.mu.Unlock()

	if inst, ok := ep.getInstance(id); ok && inst.State >= Committed {
		if inst.State == Executed {
			ep.dprint("[Execute] Already executed {%v}.", inst)
			return
		}

		ep.dprint("[Execute] {%v}.", inst)

		inst.State = Executed
		if qs, ok := ep.execWaitlist[id]; ok {
			for _, q := range qs {
				go func(q chan bool) {
					q <- true
				}(q)
			}
			delete(ep.execWaitlist, id)
		}
		ep.insertInstance(inst)
		ep.execQueue = append(ep.execQueue, ApplyMsg{
			Id:      id,
			Command: inst.Cmd.Cmd,
		})
		go func() {
			ep.execTokenChan <- true
		}()
	} else {
		panic("Executing a non-existing, not committed, or executed instance!")
	}
}

func (ep *Epaxos) applyThread() {
	for {
		<-ep.execTokenChan
		ep.mu.Lock()

		msg := ep.execQueue[0]
		ep.execQueue = ep.execQueue[1:]
		ep.applyCh <- msg

		ep.mu.Unlock()
	}
}
